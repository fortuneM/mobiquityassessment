* This Test Automation suite tests Rest APIs validations for the comments for the post made by aspecific user.
* I used a BDD Cucumber framework and Java Rest Assured to automate the rest calls.  
* i face some challenge when trying to make a request for getting a user by username, the API is not available.

	How to Install and Run the Project
1. Have intelliJ intalled
2. Install Java and Maven
3. Add Cucumber java and Gherkin plugins
4. Use Mvn Test or IntelliJ Run from TestRunner class
5. The project can also be run from the Pipeline - https://app.circleci.com/pipelines/bitbucket/fortuneM/mobiquityassessment?branch=circleci-project-setup&filter=all
6. Bitbucket - https://bitbucket.org/fortuneM/mobiquityassessment/src/master/