package pojo;

public class UsersPojo {
    int id;
    int name;
    String username;
    String email;
    String phone;
    String website;
    AddressPojo addess;
    AddressPojo.Geo geo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public AddressPojo getAddess() {
        return addess;
    }

    public void setAddess(AddressPojo addess) {
        this.addess = addess;
    }

    public AddressPojo.Geo getGeo() {
        return geo;
    }

    public void setGeo(AddressPojo.Geo geo) {
        this.geo = geo;
    }

    class AddressPojo{
        String street;
        String suite;
        String city;
        String zipcode;

        class Geo{
            String lat;
            String lng;
        }
    }

    class Company{
        String name;
        String catchPhrase;
        String bs;
    }

}
