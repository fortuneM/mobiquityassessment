package common;

import com.google.gson.JsonArray;
import groovy.json.JsonException;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pojo.CommentsPojo;
import pojo.PostsPojo;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Base {

    //Base url for the api under test
    private static final String BASE_URL = "https://jsonplaceholder.typicode.com";

    public static Response response;
    public static String jsonString;
    public static RequestSpecification request;
    public  static  int userID;
    public static  Response userResponseData;
    public static  Response userPostsData;
    public  static List<CommentsPojo> allComments;
    public  static List<PostsPojo> userPosts;
    public  static JsonPath jsonPathEvaluator;

    public void InitiateRestBaseCall(){
        //Instantiation for rest assured to the base url
        RestAssured.baseURI = BASE_URL;
        request = RestAssured.given();
    }

    public static boolean validateEMail(String emailAddres) {
        String regexPattern = "^(.+)@(\\S+)$";
        return Pattern.compile(regexPattern)
                .matcher(emailAddres)
                .matches();
    }

}
