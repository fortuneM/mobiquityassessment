package restrequests;

import com.google.gson.JsonArray;
import com.mongodb.util.JSON;
import common.Base;
import cucumber.api.java.en.Given;
import gherkin.deps.com.google.gson.JsonObject;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pojo.CommentsPojo;
import pojo.PostsPojo;
import pojo.UsersPojo;

import javax.jws.soap.SOAPBinding;
import javax.swing.*;
import java.util.Collections;
import java.util.List;

public class RestCallsObjects extends Base {

    public Response getUserByUsername(String userID) {

        //Instantiation for rest assured to the base url
        InitiateRestBaseCall();

        response = request.get("/users/" + userID);

        //Json response printout
        jsonString = response.prettyPrint();
        userResponseData = response;

        return response;
    }

    public Response getPostsByUserID() {

        //Instantiation for rest assured to the base url
        InitiateRestBaseCall();

        //Convert the response to json path to extract data
        JsonPath jsonPathEvaluator = userResponseData.jsonPath();
        userID = jsonPathEvaluator.get("id");

        //requesting posts using userID
        response = request.get("/posts/" + userID);

        //Json response printout
        jsonString = response.prettyPrint();
        userPostsData = response;

        return response;
    }

    public boolean fetchCommentsAndValidateEmailFormat() {
        //Instantiation for rest assured to the base url
        InitiateRestBaseCall();

        System.out.println("User ID" + userID);
        //requesting posts using userID
        response = request.get("/posts/" + userID + "/comments");
        //Json response printout
        jsonString = response.prettyPrint();
        if (response.statusCode() == 200) {
            jsonPathEvaluator = response.jsonPath();
            allComments = jsonPathEvaluator.getList("", CommentsPojo.class);
            return true;
        }

        return false;
    }

    public boolean validateCommentsEmail() {
        if (!allComments.isEmpty()) {
            for (CommentsPojo comments : allComments) {
                System.out.println("Comment email: " + comments.email);
                //if vaildation fails stop loop then return a false for invalid email
                if (!validateEMail(comments.email)) {
                    return false;
                }
            }
            //if all the validations are done true will be return to the Unit test block
            return true;
        }
        return false;
    }

    public boolean verifyUsersResponseData() {
        if (!userResponseData.getBody().toString().isEmpty()) {
            return true;
        }
        return false;
    }

    public boolean verifyUserPostsData() {
        if (!userPostsData.getBody().toString().isEmpty()) {
            return true;
        }
        return false;
    }
}
