@runAll
Feature: Validate comments on a post made by a specific user.
  @getUsers
  Scenario Outline: Search user with username
    Given Search for the user with username "<userID>"
    Then Verify that the user response data is not empty

    Examples:
      | usename  | userID |  |
      | Delphine | 1      |  |

  @getPosts
  Scenario Outline: search for the posts written by the user
    Given Search for posts written by the user
    Then Verify that posts response are not empty
    Examples:
      | userID |  |  |
      | 9      |  |  |

  @getComments
  Scenario Outline: fetch the comments and validate email format in the comment section
    Given Fetch the comments by user
    Then Validate email address on each post

    Examples:
      | usename  |  |  |
      | Delphine |  |  |