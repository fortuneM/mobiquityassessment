package stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import restrequests.RestCallsObjects;

public class StepDef extends RestCallsObjects {
    RestCallsObjects callsObjects = new RestCallsObjects();

    @Given("Search for the user with username \"([^\"]*)\"$")
    public void searchForTheUserWithUsername(String userID) {
        Assert.assertEquals("Unable to get user " + "",200, callsObjects.getUserByUsername(userID).getStatusCode());
    }

    @Given("^Search for posts written by the user$")
    public void searchForPostsWrittenByTheUser() {
        Assert.assertEquals("Unable to get posts by UserID " + "",200, callsObjects.getPostsByUserID().getStatusCode());
    }

    @Given("^Fetch the comments by user$")
    public void fetchTheCommentsAndValidateEmailFormat() {
        Assert.assertEquals("Unable to get comments by user " + "",true, callsObjects.fetchCommentsAndValidateEmailFormat());
    }

    @Then("^Validate email address on each post$")
    public void validateEmailAddressOnEachPost() {
        Assert.assertEquals("Unable to validate email address " + "",true, callsObjects.validateCommentsEmail());
    }

    @Then("^Verify that the user response data is not empty$")
    public void verifyThatTheUserResponseIsNotEmpty() {
        Assert.assertEquals("Unable to get user " + "",true, callsObjects.verifyUsersResponseData());
    }

    @Then("^Verify that posts response are not empty$")
    public void verifyThatPostsResponseAreNotEmpty() {
        Assert.assertEquals("Unable to get user " + "",true, callsObjects.verifyUserPostsData());
    }
}
