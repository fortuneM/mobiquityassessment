$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/ValidateComments.feature");
formatter.feature({
  "line": 2,
  "name": "Validate comments on a post made by a specific user.",
  "description": "",
  "id": "validate-comments-on-a-post-made-by-a-specific-user.",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@runAll"
    }
  ]
});
formatter.scenarioOutline({
  "line": 4,
  "name": "Search user with username",
  "description": "",
  "id": "validate-comments-on-a-post-made-by-a-specific-user.;search-user-with-username",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@getUsers"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "Search for the user with username \"\u003cuserID\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Verify that the user response data is not empty",
  "keyword": "Then "
});
formatter.examples({
  "line": 8,
  "name": "",
  "description": "",
  "id": "validate-comments-on-a-post-made-by-a-specific-user.;search-user-with-username;",
  "rows": [
    {
      "cells": [
        "usename",
        "userID",
        ""
      ],
      "line": 9,
      "id": "validate-comments-on-a-post-made-by-a-specific-user.;search-user-with-username;;1"
    },
    {
      "cells": [
        "Delphine",
        "1",
        ""
      ],
      "line": 10,
      "id": "validate-comments-on-a-post-made-by-a-specific-user.;search-user-with-username;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 10,
  "name": "Search user with username",
  "description": "",
  "id": "validate-comments-on-a-post-made-by-a-specific-user.;search-user-with-username;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@runAll"
    },
    {
      "line": 3,
      "name": "@getUsers"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "Search for the user with username \"1\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Verify that the user response data is not empty",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 35
    }
  ],
  "location": "StepDef.searchForTheUserWithUsername(String)"
});
formatter.result({
  "duration": 5453174800,
  "status": "passed"
});
formatter.match({
  "location": "StepDef.verifyThatTheUserResponseIsNotEmpty()"
});
formatter.result({
  "duration": 97500,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 13,
  "name": "search for the posts written by the user",
  "description": "",
  "id": "validate-comments-on-a-post-made-by-a-specific-user.;search-for-the-posts-written-by-the-user",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 12,
      "name": "@getPosts"
    }
  ]
});
formatter.step({
  "line": 14,
  "name": "Search for posts written by the user",
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": "Verify that posts response are not empty",
  "keyword": "Then "
});
formatter.examples({
  "line": 16,
  "name": "",
  "description": "",
  "id": "validate-comments-on-a-post-made-by-a-specific-user.;search-for-the-posts-written-by-the-user;",
  "rows": [
    {
      "cells": [
        "userID",
        "",
        ""
      ],
      "line": 17,
      "id": "validate-comments-on-a-post-made-by-a-specific-user.;search-for-the-posts-written-by-the-user;;1"
    },
    {
      "cells": [
        "9",
        "",
        ""
      ],
      "line": 18,
      "id": "validate-comments-on-a-post-made-by-a-specific-user.;search-for-the-posts-written-by-the-user;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 18,
  "name": "search for the posts written by the user",
  "description": "",
  "id": "validate-comments-on-a-post-made-by-a-specific-user.;search-for-the-posts-written-by-the-user;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 12,
      "name": "@getPosts"
    },
    {
      "line": 1,
      "name": "@runAll"
    }
  ]
});
formatter.step({
  "line": 14,
  "name": "Search for posts written by the user",
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": "Verify that posts response are not empty",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDef.searchForPostsWrittenByTheUser()"
});
formatter.result({
  "duration": 958048400,
  "status": "passed"
});
formatter.match({
  "location": "StepDef.verifyThatPostsResponseAreNotEmpty()"
});
formatter.result({
  "duration": 71400,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 21,
  "name": "fetch the comments and validate email format in the comment section",
  "description": "",
  "id": "validate-comments-on-a-post-made-by-a-specific-user.;fetch-the-comments-and-validate-email-format-in-the-comment-section",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 20,
      "name": "@getComments"
    }
  ]
});
formatter.step({
  "line": 22,
  "name": "Fetch the comments by user",
  "keyword": "Given "
});
formatter.step({
  "line": 23,
  "name": "Validate email address on each post",
  "keyword": "Then "
});
formatter.examples({
  "line": 25,
  "name": "",
  "description": "",
  "id": "validate-comments-on-a-post-made-by-a-specific-user.;fetch-the-comments-and-validate-email-format-in-the-comment-section;",
  "rows": [
    {
      "cells": [
        "usename",
        "",
        ""
      ],
      "line": 26,
      "id": "validate-comments-on-a-post-made-by-a-specific-user.;fetch-the-comments-and-validate-email-format-in-the-comment-section;;1"
    },
    {
      "cells": [
        "Delphine",
        "",
        ""
      ],
      "line": 27,
      "id": "validate-comments-on-a-post-made-by-a-specific-user.;fetch-the-comments-and-validate-email-format-in-the-comment-section;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 27,
  "name": "fetch the comments and validate email format in the comment section",
  "description": "",
  "id": "validate-comments-on-a-post-made-by-a-specific-user.;fetch-the-comments-and-validate-email-format-in-the-comment-section;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 20,
      "name": "@getComments"
    },
    {
      "line": 1,
      "name": "@runAll"
    }
  ]
});
formatter.step({
  "line": 22,
  "name": "Fetch the comments by user",
  "keyword": "Given "
});
formatter.step({
  "line": 23,
  "name": "Validate email address on each post",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDef.fetchTheCommentsAndValidateEmailFormat()"
});
formatter.result({
  "duration": 273759300,
  "status": "passed"
});
formatter.match({
  "location": "StepDef.validateEmailAddressOnEachPost()"
});
formatter.result({
  "duration": 3842700,
  "status": "passed"
});
});